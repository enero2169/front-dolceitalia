import axios from "axios";

const API_URL = "http://localhost:8000/api/v1/auth/";

const register = (firstname, lastname, email, password) => {
    return axios.post(API_URL + "signup", {
        firstname,
        lastname,
        email,
        password,
    });
};

const resetPassword = (email) => {
    return axios.post(API_URL + "forgot-password", {
        email,
    });
};

const updatePassword = (token, user_id, password) => {
    return axios.post(API_URL + "update-password", {
        token,
        user_id,
        password,
    });
};

const login = async (email, password) => {
    try {
        let response = await axios.post(API_URL + "signin", {
            email,
            password,
        });

        if (response.data.accessToken) {
            localStorage.setItem("user", JSON.stringify(response.data));
        }

        return response.data;
    } catch (error) {
        throw error;
    }
};

const logout = () => {
    localStorage.removeItem("user");
};

export default {
    register,
    resetPassword,
    updatePassword,
    login,
    logout,
};
